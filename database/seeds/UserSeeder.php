<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();

        $superadmin = Role::where('name', 'superadmin')->first();

        $user = User::create([
            'name' => 'Admin Versailles',
            'username' => 'superadmin',
            'email' => 'super@admin.com',
            'password' => bcrypt('chausiangyang'),
        ]);

        $user2 = User::create([
            'name' => 'Admin Versailles 2',
            'username' => 'superadmin12',
            'email' => 'super12@admin.com',
            'password' => bcrypt('superadmin12'),
        ]);

        $user->roles()->attach($superadmin);
        $user2->roles()->attach($superadmin);
    }
}
