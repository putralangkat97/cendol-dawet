<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Versailleslottery.com</title>
    <link rel="icon" href="{{ asset('/logo-versailles.png') }}">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha256-pasqAKBDmFT4eHoN2ndd6lN370kFiGUFyTiUHWhU7k8=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web&display=swap" rel="stylesheet">
    <script src="{{ asset('dist/libs/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('dist/libs/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('dist/libs/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('dist/libs/datatables/responsive.bootstrap4.min.js') }}"></script>
    <link href="{{ asset('dist/libs/datatables/dataTables.bootstrap4.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ asset('css/result.css') }}">
</head>
<body>
    <div class="row">
        <div class="bg">
            <a href="/" class="logo"><h1 class="text-white">Versailleslottery.com</h1></a>
            <p class="text-white latest">Latest Result</p>
            <p class="date">{{ $now }}</p>
        </div>
    </div>
    <br><br>
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12">
               <h3>History Result</h3><br>
                <table class="table table-bordered mb-4" id="dataTable">
                    <thead class="table-dark">
                        <tr class="text-center">
                            <th>Date</th>
                            <th>Time</th>
                            <th>1st Place</th>
                            <th>2nd Place</th>
                            <th>3rd Place</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($results as $row)
                        @if($row->tanggal->format('Y-m-d') != $skr && $row->tanggal->format('Y-m-d') != $tom)
                        <tr class="text-center">
                            <td>{{ $row->tanggal->format('d-m-Y') }}</td>
                            <td>{{ $row->waktu }}</td>
                            <td>{{ $row->no_satu }}</td>
                            <td>{{ $row->no_dua }}</td>
                            <td>{{ $row->no_tiga }}</td>
                        </tr>
                        @endif
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function(){
            $('#dataTable').DataTable({
               'ordering': false
            });
        });
    </script>
</body>
</html>
