<!doctype html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Page Title -->
    <title>Versailleslottery.com</title>
    <!-- Favicon -->
    <link rel="icon" href="{{ asset('/logo-versailles.png') }}">
    <!-- <link rel="icon" href="http://themesindustry.com/html/wexim-2020/images/favicon.ico"> -->
    <!-- Animate -->
    <link rel="stylesheet" href="http://themesindustry.com/html/wexim-2020/css/animate.min.css">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="http://themesindustry.com/html/wexim-2020/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="http://themesindustry.com/html/wexim-2020/css/font-awesome.min.css">
    <!-- Owl Carousel -->
    <link rel="stylesheet" href="http://themesindustry.com/html/wexim-2020/css/owl.carousel.min.css">
    <link rel="stylesheet" href="http://themesindustry.com/html/wexim-2020/css/owl.theme.default.min.css">
    <!-- Cube Portfolio -->
    <link rel="stylesheet" href="http://themesindustry.com/html/wexim-2020/css/cubeportfolio.min.css">
    <!-- Fancy Box -->
    <link rel="stylesheet" href="http://themesindustry.com/html/wexim-2020/css/jquery.fancybox.min.css">
    <!-- REVOLUTION STYLE SHEETS -->
    <link rel="stylesheet" type="text/css" href="http://themesindustry.com/html/wexim-2020/rs-plugin/css/settings.css">
    <link rel="stylesheet" type="text/css" href="http://themesindustry.com/html/wexim-2020/rs-plugin/css/navigation.css">
    <!-- Style Sheet -->
    <link rel="stylesheet" href="http://themesindustry.com/html/wexim-2020/css/style.css">
    <!-- Style Customizer's stylesheets -->
    <link rel="stylesheet" type="text/css" href="http://themesindustry.com/html/wexim-2020/color-switcher/js/style-customizer/css/style-customizer.css">
    <link rel="stylesheet/less" type="text/css" href="http://themesindustry.com/html/wexim-2020/color-switcher/less/skin.less">
    <link rel="stylesheet" href="{{ asset('fontawesome-free/css/all.css') }}">


</head>
<body data-spy="scroll" data-target=".navbar" data-offset="90">
<style>
    .win {
        margin-top: 0px;
        background-color: white 0.5;
    }
    .mt-lg-5, .my-lg-5 {
        margin-top: 0rem !important;
    }
    .hilang {
        opacity: 0;
    }
    .gradient-bg1 {
        background-image: url('{{ asset("images/bg9.jpeg") }}');
        background-attachment: scroll;
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
    }
    .bg-center {
        background-image: url('{{ asset("images/bg3.jpeg") }}');
        background-attachment: scroll;
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
    }
    .past {
        background-image: url('{{ asset("images/bg8.jpeg") }}');
        background-attachment: scroll;
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
    }
      header a.logo img {
         max-height: 65px;
         max-width: 100px;
         vertical-align: sub;
      }

      @media screen and (max-width: 768px) {
         header a.logo img {
            max-height: 55px;
            max-width: 100px;
            vertical-align: sub;
         }

         .float-left {
             float: left;
             height: 27px;
         }

         .float-right {
             float: right;
             height: 17px;
         }

         .mobile {
             height: 13px;
         }

         .tanggal {
             font-size: 12px;
         }
         .table td, .table th {
            padding: .5rem;
         }
      }
</style>

<!--Loader Start-->
<!-- <div class="loader">
    <div class="loader-inner">
        <div class="loader-blocks">
            <span class="block-1"></span>
            <span class="block-2"></span>
            <span class="block-3"></span>
            <span class="block-4"></span>
            <span class="block-5"></span>
            <span class="block-6"></span>
            <span class="block-7"></span>
            <span class="block-8"></span>
            <span class="block-9"></span>
            <span class="block-10"></span>
            <span class="block-11"></span>
            <span class="block-12"></span>
            <span class="block-13"></span>
            <span class="block-14"></span>
            <span class="block-15"></span>
            <span class="block-16"></span>
        </div>
    </div>
</div> -->
<!--Loader End-->

<!--Header Start-->
<header class="cursor-light">

    <!--Navigation-->
    <nav class="navbar navbar-top-default navbar-expand-lg navbar-gradient nav-icon">
        <div class="container">
            <a href="/" title="Logo" class="logo link">
                <!--Logo Default-->
                <img src="{{ asset('/logo-versailles.png') }}" alt="logo" class="logo-dark default">
            </a>

            <!--Nav Links-->
            <div class="collapse navbar-collapse" id="wexim">
                <div class="navbar-nav ml-auto">
                        <!-- <a class="nav-link link scroll" href="#home">Home</a>
                        <a class="nav-link link scroll" href="#about">About</a>
                        <a class="nav-link link scroll" href="#team">Team</a>
                        <a class="nav-link link scroll" href="#portfolio">Work</a>
                        <a class="nav-link link scroll" href="#price">Pricing</a>
                        <a class="nav-link link scroll" href="#blog">Blog</a> -->
                        <a href="{{ url('/result') }}" class="nav-link link btn btn-large btn-rounded btn-white text-dark" target="_blank">Result</a>
                    <!-- <span class="menu-line"><i class="fa fa-angle-down" aria-hidden="true"></i></span> -->
                </div>
            </div>

            <!--Side Menu Button-->
            <a href="javascript:void(0)" class="d-md-block d-lg-none parallax-btn sidemenu_btn" id="sidemenu_toggle">
                <div class="animated-wrap sidemenu_btn_inner">
                    <div class="animated-element">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
            </a>

        </div>
    </nav>

    <!--Side Nav-->
    <div class="side-menu">
        <div class="inner-wrapper">
            <span class="btn-close link" id="btn_sideNavClose"></span>
            <nav class="side-nav w-100">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link link btn btn-large btn-rounded " href="{{ url('/result') }}">Result</a>
                    </li>
                </ul>
            </nav>

            <!-- <div class="side-footer text-white w-100">
                <ul class="social-icons-simple">
                    <li class="animated-wrap"><a class="animated-element" href="javascript:void(0)"><i class="fa fa-facebook"></i> </a> </li>
                    <li class="animated-wrap"><a class="animated-element" href="javascript:void(0)"><i class="fa fa-instagram"></i> </a> </li>
                    <li class="animated-wrap"><a class="animated-element" href="javascript:void(0)"><i class="fa fa-twitter"></i> </a> </li>
                </ul>
                <p class="text-white">&copy; 2019 Wexim. Made With Love by Themesindustry</p>
            </div> -->
        </div>
    </div>
    <a id="close_side_menu" href="javascript:void(0);"></a>
    <!-- End side menu -->

</header>
<!--Header end-->

<!--slider-->
<section id="home" class="cursor-light p-0">
    <h2 class="d-none">hidden</h2>
    <div id="rev_slider_19_1_wrapper" class="rev_slider_wrapper fullscreen-container" data-alias="wexim_slider_01" data-source="gallery" style="background:transparent;padding:0px;">
        <!-- START REVOLUTION SLIDER 5.4.8.1 fullscreen mode -->
        <div id="rev_slider_19_1" class="rev_slider fullscreenbanner" style="display:none;" data-version="5.4.8.1">
            <ul>
                <!-- SLIDE  -->
                <li data-index="rs-1" data-transition="crossfade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="default"  data-thumb="images/slide-img1.jpg"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <!-- MAIN IMAGE -->
                    <!-- <img src="http://themesindustry.com/html/wexim-2020/images/slider-image1.jpg"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina> -->
                    <!-- LAYERS -->

                    <!-- Overlay -->
                    <div class="gradient-bg1 bg-overlay opacity-9 z-index-1"></div>

                    <!-- LAYER NR. 1 -->
                    <!-- <div class="tp-caption   tp-resizeme"
                         id="slide-91-layer-1"
                         data-x="['left','left','center','center']" data-hoffset="['0','50','0','0']"
                         data-y="['top','top','middle','middle']" data-voffset="['309','262','-130','-130']"
                         data-fontsize="['16','16','16','16']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"

                         data-type="text"
                         data-responsive_offset="on"

                         data-frames='[{"delay":200,"speed":900,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":900,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['inherit','inherit','center','center']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"

                         style="z-index: 5; white-space: nowrap; font-size: 16px; line-height: 20px; font-weight: 300; color: #ffffff; letter-spacing: 0px;font-family:Roboto;"><p class="text-white">Lorem ipsuM jolor sit amet purus.  </p></div> -->

                    <!-- LAYER NR. 2 -->
                    <div class="tp-caption   tp-resizeme"
                         id="slide-91-layer-2"
                         data-x="['left','left','center','center']" data-hoffset="['0','50','0','0']"
                         data-y="['top','top','top','top']" data-voffset="['357','309','383','255']"
                         data-fontsize="['40','40','40','35']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"

                         data-type="text"
                         data-responsive_offset="on"

                         data-frames='[{"delay":720,"split":"chars","splitdelay":0.1,"speed":900,"split_direction":"forward","frame":"0","from":"sX:0.8;sY:0.8;opacity:0;","to":"o:1;","ease":"Power4.easeOut"},{"delay":"wait","speed":900,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['inherit','inherit','center','center']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"

                         style="z-index: 6; white-space: nowrap; font-size: 40px; line-height: 50px; font-weight: 500; color: #ffffff; letter-spacing: 0px;font-family:Poppins;">Next Drawing <i class="fa fa-arrow-right"></i> <span id="final"></span> </div>

                    <!-- LAYER NR. 3 -->
                    <div class="tp-caption tp-resizeme"
                         id="slide-91-layer-3"
                         data-x="['left','left','center','center']" data-hoffset="['0','50','0','0']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['0','0','-15','-30']"
                         data-fontsize="['40','40','40','35']"

                         data-whitespace="nowrap"

                         data-type="text"
                         data-responsive_offset="on"

                         data-frames='[{"delay":1290,"speed":900,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":900,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['inherit','inherit','center','center']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"

                         style="z-index: 7; white-space: nowrap; font-size: 40px; line-height: 50px; font-weight: 200; color: #ffffff; letter-spacing: 0px;font-family:Poppins;margin-left: -8px;"><span class="hilang">.</span><span id="jam"></span></div>

                    <!-- LAYER NR. 4 -->
                    <div class="tp-caption tp-resizeme"
                         id="slide-91-layer-4"
                         data-x="['left','left','center','center']" data-hoffset="['0','50','1','-1']"
                         data-y="['top','top','middle','middle']" data-voffset="['484','447','60','45']"
                         data-fontsize="['16','16','16','16']"
                         data-lineheight="['22','22','22','18']"
                         data-width="['601','530','700','450']"
                         data-height="none"
                         data-whitespace="normal"

                         data-type="text"
                         data-responsive_offset="on"

                         data-frames='[{"delay":1680,"speed":900,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":900,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['inherit','inherit','center','center']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"

                         style="z-index: 8; min-width: 601px; max-width: 601px; white-space: normal; font-size: 30px; line-height: 22px; font-weight: bold; color: #ffffff; letter-spacing: 1px;font-family:Roboto;"><p class="text-white">Draws Daily | Draws Time: 00:00 AM | 04:00 AM | 08:00 AM | 12:00 PM</p>
                        </div>


                        <div class="rs-looped rs-wave"  data-speed="20" data-angle="0" data-radius="8px" data-origin="50% 50%">
                        </div>

                    </div>
                </li>
            </ul>
    </div>

    <!--slider social-->

    <!--scroll down-->
    <a href="#about" class="scroll-down link scroll">Scroll Down <i class="fa fa-arrow-down"></i></a>

</section>
<!--slider end-->

<!--About Start-->
<section id="about" class="bg-light bg-center">
    <div class="container">
        <!--About-->
        <div class="row align-items-center wow fadeIn">
            <div class="col-md-6">
                <div class="title">
                    <h2 class="third-color mb-3">Latest Winner</h2>
                    @foreach($data as $row)
                        @for ($i=0; $i <= 3; $i++)
                        <img src="{{asset('/images/'.$row->no_satu[$i].'.png')}}" alt="" width="auto" height="100px">
                        @endfor
                        <br><br>
                    <h3 class="waktu text-white">{{ $row->waktu }}</h3>
                </div>
            </div>
        </div>
        <!--Features-->
        <div class="row mt-lg-5 win">
            <div class="col-md-4 wow fadeInLeft">
                <div class="feature-box">
                    <span class="item-number gradient-text1">
                        1st
                    </span>
                    <h6 class="mb-4">
                    @for ($i=0; $i <= 3; $i++)
                    <img src="{{asset('/images/'.$row->no_satu[$i].'.png')}}" alt="" width="auto" height="60px">
                    @endfor
                    </h6>

                </div>
            </div>
            <div class="col-md-4 wow fadeInUp">
                <div class="feature-box">
                    <span class="item-number gradient-text1">
                        2nd
                    </span>
                    <h6 class="mb-4">
                    @for ($i=0; $i <= 3; $i++)
                    <img src="{{asset('/images/'.$row->no_dua[$i].'.png')}}" alt="" width="auto" height="60px">
                    @endfor
                    </h6>

                </div>
            </div>
            <div class="col-md-4 wow fadeInRight">
                <div class="feature-box">
                    <span class="item-number gradient-text1">
                        3rd
                    </span>
                    <h6 class="mb-4">
                    @for ($i=0; $i <= 3; $i++)
                    <img src="{{asset('/images/'.$row->no_tiga[$i].'.png')}}" alt="" width="auto" height="60px">
                    @endfor
                    </h6>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
<!--About End-->

<!--Team Start-->
<section id="team" class="past">
    <div class="container">
        <!--Heading-->
        <div class="row wow fadeIn">
            <div class="col-md-12 text-center">
                <div class="title d-inline-block">
                    <h2 class="text-white mb-3">Past Winning Number</h2>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <table class="table table-bordered table-striped">
                    <thead class="text-center table-dark">
                        <tr>
                            <th class="tanggal">Date</th>
                            <th class="tanggal">00:00 AM</th>
                            <th class="tanggal">04:00 AM</th>
                            <th class="tanggal">08:00 AM</th>
                            <th class="tanggal">12:00 PM</th>
                        </tr>
                    </thead>
                    <tbody class="table-light text-center">
                       <tr>
                          <td class="tanggal">{{ $tadi }}</td>
                          <td>
                          @if($pagiTadi == null)
                            -
                          @else
                          @for ($i=0; $i <= 3; $i++)
                            <img class="mobile" src="{{asset('/images/'.$pagiTadi[$i].'.png')}}" alt="" width="auto" height="30px">
                          @endfor
                          @endif
                          </td>
                          <td>
                          @if($siangTadi == null)
                            -
                          @else
                          @for ($i=0; $i <= 3; $i++)
                            <img class="mobile" src="{{asset('/images/'.$siangTadi[$i].'.png')}}" alt="" width="auto" height="30px">
                          @endfor
                          @endif
                          </td>
                          <td>
                          @if($soreTadi == null)
                            -
                          @else
                          @for ($i=0; $i <= 3; $i++)
                            <img class="mobile" src="{{asset('/images/'.$soreTadi[$i].'.png')}}" alt="" width="auto" height="30px">
                          @endfor
                          @endif
                          </td>
                          <td>
                          @if($malamTadi == null)
                            -
                          @else
                          @for ($i=0; $i <= 3; $i++)
                            <img class="mobile" src="{{asset('/images/'.$malamTadi[$i].'.png')}}" alt="" width="auto" height="30px">
                          @endfor
                          @endif
                          </td>
                       </tr>
                        <tr>
                            <td class="tanggal"><b>{{ $ytd }}</b></td>
                            @foreach($pagi as $pg)
                            @if($pg->tanggal->format('M j') == $ytd)
                            <td>
                                @for ($i=0; $i <= 3; $i++)
                                <img class="mobile" src="{{asset('/images/'.$pg->no_satu[$i].'.png')}}" alt="" width="auto" height="30px">
                                @endfor
                            </td>
                            @endif
                            @endforeach
                            @foreach($siang as $sg)
                            @if($sg->tanggal->format('M j') == $ytd)
                            <td>
                                @for ($i=0; $i <= 3; $i++)
                                <img class="mobile" src="{{asset('/images/'.$sg->no_satu[$i].'.png')}}" alt="" width="auto" height="30px">
                                @endfor
                            </td>
                            @endif
                            @endforeach
                            @foreach($sore as $sr)
                            @if($sr->tanggal->format('M j') == $ytd)
                            <td>
                                @for ($i=0; $i <= 3; $i++)
                                <img class="mobile" src="{{asset('/images/'.$sr->no_satu[$i].'.png')}}" alt="" width="auto" height="30px">
                                @endfor
                            </td>
                            @endif
                            @endforeach
                            @foreach($malam as $mm)
                            @if($mm->tanggal->format('M j') == $ytd)
                            <td>
                                @for ($i=0; $i <= 3; $i++)
                                <img class="mobile" src="{{asset('/images/'.$mm->no_satu[$i].'.png')}}" alt="" width="auto" height="30px">
                                @endfor
                            </td>
                            @endif
                            @endforeach
                        </tr>
                        <tr>
                            <td class="tanggal"><b>{{ $ytd2 }}</b></td>
                            @foreach($pagi as $pg)
                            @if($pg->tanggal->format('M j') == $ytd2)
                            <td>
                                @for ($i=0; $i <= 3; $i++)
                                <img class="mobile" src="{{asset('/images/'.$pg->no_satu[$i].'.png')}}" alt="" width="auto" height="30px">
                                @endfor
                            </td>
                            @endif
                            @endforeach
                            @foreach($siang as $sg)
                            @if($sg->tanggal->format('M j') == $ytd2)
                            <td>
                                @for ($i=0; $i <= 3; $i++)
                                <img class="mobile" src="{{asset('/images/'.$sg->no_satu[$i].'.png')}}" alt="" width="auto" height="30px">
                                @endfor
                            </td>
                            @endif
                            @endforeach
                            @foreach($sore as $sr)
                            @if($sr->tanggal->format('M j') == $ytd2)
                            <td>
                                @for ($i=0; $i <= 3; $i++)
                                <img class="mobile" src="{{asset('/images/'.$sr->no_satu[$i].'.png')}}" alt="" width="auto" height="30px">
                                @endfor
                            </td>
                            @endif
                            @endforeach
                            @foreach($malam as $mm)
                            @if($mm->tanggal->format('M j') == $ytd2)
                            <td>
                                @for ($i=0; $i <= 3; $i++)
                                <img class="mobile" src="{{asset('/images/'.$mm->no_satu[$i].'.png')}}" alt="" width="auto" height="30px">
                                @endfor
                            </td>
                            @endif
                            @endforeach
                        </tr>
                        <tr>
                            <td class="tanggal"><b>{{ $ytd3 }}</b></td>
                            @foreach($pagi as $pg)
                            @if($pg->tanggal->format('M j') == $ytd3)
                            <td>
                                @for ($i=0; $i <= 3; $i++)
                                <img class="mobile" src="{{asset('/images/'.$pg->no_satu[$i].'.png')}}" alt="" width="auto" height="30px">
                                @endfor
                            </td>
                            @endif
                            @endforeach
                            @foreach($siang as $sg)
                            @if($sg->tanggal->format('M j') == $ytd3)
                            <td>
                                @for ($i=0; $i <= 3; $i++)
                                <img class="mobile" src="{{asset('/images/'.$sg->no_satu[$i].'.png')}}" alt="" width="auto" height="30px">
                                @endfor
                            </td>
                            @endif
                            @endforeach
                            @foreach($sore as $sr)
                            @if($sr->tanggal->format('M j') == $ytd3)
                            <td>
                                @for ($i=0; $i <= 3; $i++)
                                <img class="mobile" src="{{asset('/images/'.$sr->no_satu[$i].'.png')}}" alt="" width="auto" height="30px">
                                @endfor
                            </td>
                            @endif
                            @endforeach
                            @foreach($malam as $mm)
                            @if($mm->tanggal->format('M j') == $ytd3)
                            <td>
                                @for ($i=0; $i <= 3; $i++)
                                <img class="mobile" src="{{asset('/images/'.$mm->no_satu[$i].'.png')}}" alt="" width="auto" height="30px">
                                @endfor
                            </td>
                            @endif
                            @endforeach
                        </tr>
                        <tr>
                            <td class="tanggal"><b>{{ $ytd4 }}</b></td>
                            @foreach($pagi as $pg)
                            @if($pg->tanggal->format('M j') == $ytd4)
                            <td>
                                @for ($i=0; $i <= 3; $i++)
                                <img class="mobile" src="{{asset('/images/'.$pg->no_satu[$i].'.png')}}" alt="" width="auto" height="30px">
                                @endfor
                            </td>
                            @endif
                            @endforeach
                            @foreach($siang as $sg)
                            @if($sg->tanggal->format('M j') == $ytd4)
                            <td>
                                @for ($i=0; $i <= 3; $i++)
                                <img class="mobile" src="{{asset('/images/'.$sg->no_satu[$i].'.png')}}" alt="" width="auto" height="30px">
                                @endfor
                            </td>
                            @endif
                            @endforeach
                            @foreach($sore as $sr)
                            @if($sr->tanggal->format('M j') == $ytd4)
                            <td>
                                @for ($i=0; $i <= 3; $i++)
                                <img class="mobile" src="{{asset('/images/'.$sr->no_satu[$i].'.png')}}" alt="" width="auto" height="30px">
                                @endfor
                            </td>
                            @endif
                            @endforeach
                            @foreach($malam as $mm)
                            @if($mm->tanggal->format('M j') == $ytd4)
                            <td>
                                @for ($i=0; $i <= 3; $i++)
                                <img class="mobile" src="{{asset('/images/'.$mm->no_satu[$i].'.png')}}" alt="" width="auto" height="30px">
                                @endfor
                            </td>
                            @endif
                            @endforeach
                        </tr>
                        <tr>
                            <td class="tanggal"><b>{{ $ytd5 }}</b></td>
                            @foreach($pagi as $pg)
                            @if($pg->tanggal->format('M j') == $ytd5)
                            <td>
                                @for ($i=0; $i <= 3; $i++)
                                <img class="mobile" src="{{asset('/images/'.$pg->no_satu[$i].'.png')}}" alt="" width="auto" height="30px">
                                @endfor
                            </td>
                            @endif
                            @endforeach
                            @foreach($siang as $sg)
                            @if($sg->tanggal->format('M j') == $ytd5)
                            <td>
                                @for ($i=0; $i <= 3; $i++)
                                <img class="mobile" src="{{asset('/images/'.$sg->no_satu[$i].'.png')}}" alt="" width="auto" height="30px">
                                @endfor
                            </td>
                            @endif
                            @endforeach
                            @foreach($sore as $sr)
                            @if($sr->tanggal->format('M j') == $ytd5)
                            <td>
                                @for ($i=0; $i <= 3; $i++)
                                <img class="mobile" src="{{asset('/images/'.$sr->no_satu[$i].'.png')}}" alt="" width="auto" height="30px">
                                @endfor
                            </td>
                            @endif
                            @endforeach
                            @foreach($malam as $mm)
                            @if($mm->tanggal->format('M j') == $ytd5)
                            <td>
                                @for ($i=0; $i <= 3; $i++)
                                <img class="mobile" src="{{asset('/images/'.$mm->no_satu[$i].'.png')}}" alt="" width="auto" height="30px">
                                @endfor
                            </td>
                            @endif
                            @endforeach
                        </tr>
                        <tr>
                            <td class="tanggal"><b>{{ $ytd6 }}</b></td>
                            @foreach($pagi as $pg)
                            @if($pg->tanggal->format('M j') == $ytd6)
                            <td>
                                @for ($i=0; $i <= 3; $i++)
                                <img class="mobile" src="{{asset('/images/'.$pg->no_satu[$i].'.png')}}" alt="" width="auto" height="30px">
                                @endfor
                            </td>
                            @endif
                            @endforeach
                            @foreach($siang as $sg)
                            @if($sg->tanggal->format('M j') == $ytd6)
                            <td>
                                @for ($i=0; $i <= 3; $i++)
                                <img class="mobile" src="{{asset('/images/'.$sg->no_satu[$i].'.png')}}" alt="" width="auto" height="30px">
                                @endfor
                            </td>
                            @endif
                            @endforeach
                            @foreach($sore as $sr)
                            @if($sr->tanggal->format('M j') == $ytd6)
                            <td>
                                @for ($i=0; $i <= 3; $i++)
                                <img class="mobile" src="{{asset('/images/'.$sr->no_satu[$i].'.png')}}" alt="" width="auto" height="30px">
                                @endfor
                            </td>
                            @endif
                            @endforeach
                            @foreach($malam as $mm)
                            @if($mm->tanggal->format('M j') == $ytd6)
                            <td>
                                @for ($i=0; $i <= 3; $i++)
                                <img class="mobile" src="{{asset('/images/'.$mm->no_satu[$i].'.png')}}" alt="" width="auto" height="30px">
                                @endfor
                            </td>
                            @endif
                            @endforeach
                        </tr>
                    </tbody>
                </table>
                <img src="{{ asset('/images/bnv.png') }}" alt="" class="float-left" height="45">
                <img src="{{ asset('/images/vip.png') }}" alt="" class="float-right">
            </div>
        </div>

        <!--Team-->
    </div>
</section>
<!--Team Start-->

<!-- Optional JavaScript -->
<script src="http://themesindustry.com/html/wexim-2020/js/jquery-3.3.1.min.js"></script>
<script src="http://themesindustry.com/html/wexim-2020/js/popper.min.js"></script>
<script src="http://themesindustry.com/html/wexim-2020/js/bootstrap.min.js"></script>
<script src="http://themesindustry.com/html/wexim-2020/js/jquery.appear.js"></script>
<!-- isotop gallery -->
<script src="http://themesindustry.com/html/wexim-2020/js/isotope.pkgd.min.js"></script>
<!-- cube portfolio gallery -->
<script src="http://themesindustry.com/html/wexim-2020/js/jquery.cubeportfolio.min.js"></script>
<!-- owl carousel slider -->
<script src="http://themesindustry.com/html/wexim-2020/js/owl.carousel.min.js"></script>
<!-- text rotate -->
<script src="http://themesindustry.com/html/wexim-2020/js/morphext.min.js"></script>
<!-- particles -->
<script src="http://themesindustry.com/html/wexim-2020/js/particles.min.js"></script>
<!-- parallax Background -->
<script src="http://themesindustry.com/html/wexim-2020/js/parallaxie.min.js"></script>
<!-- fancybox popup -->
<script src="http://themesindustry.com/html/wexim-2020/js/jquery.fancybox.min.js"></script>
<!-- wow animation -->
<script src="http://themesindustry.com/html/wexim-2020/js/wow.js"></script>
<!-- tween max animation -->
<script src="http://themesindustry.com/html/wexim-2020/js/TweenMax.min.js"></script>
<!-- REVOLUTION JS FILES -->
<script src="http://themesindustry.com/html/wexim-2020/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script src="http://themesindustry.com/html/wexim-2020/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<!-- SLIDER REVOLUTION EXTENSIONS -->
<script src="http://themesindustry.com/html/wexim-2020/rs-plugin/js/extensions/revolution.extension.actions.min.js"></script>
<script src="http://themesindustry.com/html/wexim-2020/rs-plugin/js/extensions/revolution.extension.carousel.min.js"></script>
<script src="http://themesindustry.com/html/wexim-2020/rs-plugin/js/extensions/revolution.extension.kenburn.min.js"></script>
<script src="http://themesindustry.com/html/wexim-2020/rs-plugin/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script src="http://themesindustry.com/html/wexim-2020/rs-plugin/js/extensions/revolution.extension.migration.min.js"></script>
<script src="http://themesindustry.com/html/wexim-2020/rs-plugin/js/extensions/revolution.extension.navigation.min.js"></script>
<script src="http://themesindustry.com/html/wexim-2020/rs-plugin/js/extensions/revolution.extension.parallax.min.js"></script>
<script src="http://themesindustry.com/html/wexim-2020/rs-plugin/js/extensions/revolution.extension.slideanims.min.js"></script>
<script src="http://themesindustry.com/html/wexim-2020/rs-plugin/js/extensions/revolution.extension.video.min.js"></script>
<!-- map -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAhrdEzlfpnsnfq4MgU1e1CCsrvVx2d59s"></script>
<script src="http://themesindustry.com/html/wexim-2020/js/map.js"></script>
<!-- Color Switcher -->
<script src="http://themesindustry.com/html/wexim-2020/color-switcher/js/less/less.min.js" data-env="development"></script>
<script src="http://themesindustry.com/html/wexim-2020/color-switcher/js/style-customizer/js/style-customizer.js"></script>
<!-- custom script -->
<script src="http://themesindustry.com/html/wexim-2020/js/script.js"></script>

<script>
//    function addLeadingZero(n)
//    {
//       return n < 10 ? '0' + n : n;
//    }

   window.onload = function()
   {
      windTheClock(2);
   }

   function windTheClock(timeZoneOffset)
   {
    var dt = new Date();
    dt.setHours(dt.getUTCHours() + timeZoneOffset);

    var hours = parseInt(dt.getHours() * 3600);
    var min =  parseInt(dt.getMinutes() * 60);
    var sec = parseInt(dt.getSeconds());
    var times = hours+min+sec;
    // var waktu = "";

    var waktumax = "";

    if(times >= 0 && times <=14399)
    {
        waktumax = 14399;

      waktu = '04:00 AM';
    document.getElementById('final').innerHTML = waktu;
    }
    else if(times >= 14400 && times <= 28799)
    {
        waktumax = 28799;

      waktu = '08:00 AM';
        document.getElementById('final').innerHTML = waktu;

    }
    else if(times >= 28800 && times <=43199)
    {
        waktumax = 43199;

      waktu = '12:00 PM';
    document.getElementById('final').innerHTML = waktu;
    }
    else if(times >= 43200 && times <=86399)
    {
        waktumax = 86399;

        waktu = '00:00 AM';
        document.getElementById('final').innerHTML = waktu;
    }


    var upgradeTime =  waktumax - times;

    var days        = Math.floor(upgradeTime/24/60/60);
    var hoursLeft   = Math.floor((upgradeTime) - (days * 86400));
    var hrs       = Math.floor(hoursLeft/3600);
    var minutesLeft = Math.floor((hoursLeft) - (hrs * 3600));
    var mnt     = Math.floor(minutesLeft/60);
    var remainingSeconds = upgradeTime % 60;
    function pad(n) {
        return (n < 10 ? "0" + n : n);
    }
    document.getElementById('jam').innerHTML = pad(hrs) + ":" + pad(mnt) + ":" + pad(remainingSeconds);

    if (upgradeTime == 0) {
        clearInterval(countdownTimer);
        document.getElementById('jam').innerHTML = "Refresh your browser";
    } else {
        upgradeTime--;
    }


      setTimeout(function()
      {
        windTheClock(timeZoneOffset)
      }, 1000);
   }
</script>

</body>

<!-- Mirrored from themesindustry.com/html/wexim-2020/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 31 Jan 2020 05:10:23 GMT -->
</html>
