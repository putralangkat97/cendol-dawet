@extends('admin.layouts.masters')
@section('content')
<div class="row">
    <div class="col-12">
        <div class="card-box table-responsive">
        <button type="button" name="create_record" id="create_record" class="btn btn-primary btn-sm sub-header text-white"><i class="fa fa-plus"></i></button>
            <div class="table-rep-plugin">
                <div class="mb-0" data-pattern="priority-columns">
                    <table id="number-table" class="table table-bordered dt-responsive nowrap" style="width: 100%;text-align: center;">
                        <thead>
                            <tr>
                                <th class="text-center">Tanggal</th>
                                <th class="text-center">Waktu</th>
                                <th class="text-center">1st Place</th>
                                <th class="text-center">2nd Place</th>
                                <th class="text-center">3rd Place</th>
                                <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody class="text-center">
                        @foreach($number as $key => $n)
                            <tr>
                                <td>{{ date('d-m-Y', strtotime($n->tanggal)) }}</td>
                                <td>{{ $n->waktu }}</td>
                                <td>{{ $n->no_satu }}</td>
                                <td>{{ $n->no_dua }}</td>
                                <td>{{ $n->no_tiga }}</td>
                                <td>
                                    <a href="#" class="edit" id="{{ $n->id }}"><i class="fa fa-edit text-info"></i></a>
                                    &nbsp;
                                    <a href="#" class="delete" onclick="deleteConfirmation({{ $n->id }})"><i class="fa fa-trash text-danger"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end row -->

<!-- Modal Add -->
<div id="formModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add New</h4>
            </div>
            <div class="modal-body">
                <span id="form_result"></span>
                <form method="post" id="sample_form" class="form-horizontal">
                @csrf
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label class="control-label">Select Date:</label>
                                <input type="text" name="tanggal" id="tanggal" class="form-control datepicker" placeholder="Select Date" autocomplete="off">
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label class="control-label">Time:</label>
                                <select name="waktu" id="waktu" class="form-control" autocomplete="off">
                                    <option value="">Select Time</option>
                                    <option value="00:00 AM">00:00 AM</option>
                                    <option value="04:00 AM">04:00 AM</option>
                                    <option value="08:00 AM">08:00 AM</option>
                                    <option value="12:00 PM">12:00 PM</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label class="control-label">1st Winner:</label>
                                <input type="text" name="no_satu" id="no_satu" class="form-control" onkeypress="return isNumberKey(event)" maxlength="4" minlength="4" placeholder="0000" autocomplete="off">
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label class="control-label">2nd Winner:</label>
                                <input type="text" name="no_dua" id="no_dua" class="form-control" onkeypress="return isNumberKey(event)" maxlength="4" minlength="4" placeholder="0000" autocomplete="off">
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label class="control-label">3rd Winner:</label>
                                <input type="text" name="no_tiga" id="no_tiga" class="form-control" onkeypress="return isNumberKey(event)" maxlength="4" minlength="4" placeholder="0000" autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group" align="center">
                                <input type="hidden" name="action" id="action" value="Add">
                                <input type="hidden" name="hidden_id" id="hidden_id">
                                <input type="submit" name="action_button" value="Add" id="action_button" class="btn btn-primary btn-block">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function(){
    $('#number-table').DataTable({
      'ordering': false,
   });


    $('.datepicker').datepicker({
        uiLibrary: 'bootstrap',
        format: 'yyyy-mm-dd'
    });

    $('#create_record').click(function(){
        $('.modal-title').text('Add New Record');
        $('#action_button').val('Add');
        $('#action').val('Add');
        $('#form_result').html('');
        $('#formModal').modal('show');
    });

    $('#sample_form').on('submit', function(event){
        event.preventDefault();
        var id = $(this).attr('id');
        var action_url  = '';

        if($('#action').val() == 'Add')
        {
            action_url = "{{ route('admin.add') }}";
        }

        if($('#action').val() == 'Edit')
        {
            action_url = "{{ route('admin.update') }}";
        }

        $.ajax({
            url: action_url,
            method: "POST",
            data: $(this).serialize(),
            dataType: 'json',
            success: function(data)
            {
                var html = '';
                if(data.errors)
                {
                    html = '<div class="alert alert-danger">';
                    for(var count = 0; count < data.errors.length; count++)
                    {
                        html += '<li><b>'+data.errors[count]+'</b></li>';
                    }
                    html += '</div>';
                }

                if (data.error) {
                    Swal.fire({
                        icon: 'error',
                        title: "Time already exist!",
                        text: "Change your time option",
                    });
                }

                if(data.success)
                {
                    Swal.fire({
                        // position: "top",
                        // toast: true,
                        icon: 'success',
                        title: "Success",
                        text: "Data save successfully!",
                        // showConfirmButton: false,
                        // timer: 3000
                    }).then(function() {
                        location.reload();
                    });
                    $('#formModal').modal('hide');
                    // html = '<div class="alert alert-success">'+data.success+'</div>';
                    $('#sample_form')[0].reset();
                }
                $('#form_result').html(html);
            }
        });
    });

    $(document).on('click', '.edit', function(){
        var id = $(this).attr('id');
        $('#form_result').html('');
        $.ajax({
            url: '/admin_zone/'+id+'/edit',
            dataType: 'json',
            success: function(data)
            {
                $('#tanggal').val(data.result.tanggal);
                $('#waktu').val(data.result.waktu);
                $('#no_satu').val(data.result.no_satu);
                $('#no_dua').val(data.result.no_dua);
                $('#no_tiga').val(data.result.no_tiga);
                $('#hidden_id').val(data.result.id);
                $('.modal-title').text('Edit Record');
                $('#action_button').val('Edit');
                $('#action').val('Edit');
                $('#formModal').modal('show');
            }
        });
    });
});
function deleteConfirmation(id) {
    Swal.fire({
        icon: 'warning',
        title: "Delete?",
        text: "Please ensure and then confirm!",
        type: "warning",
        showCancelButton: !0,
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, Cancel it!",
        reverseButtons: !0
    }).then(function (event) {

        if (event.value === true) {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

            $.ajax({
                type: 'POST',
                url: '/admin_zone/destroy/'+id,
                data: { _token: CSRF_TOKEN },
                dataType: 'JSON',
                success: function (results) {
                    if (results.success === true) {
                        Swal.fire({
                            // toast: true,
                            icon: 'success',
                            title: "Success",
                            text: "Data Deleted",
                            // showConfirmButton: false,
                            // timer: 1500
                        }).then(function() {
                            location.reload();
                        });
                        $('#sample_form')[0].reset();
                    } else {
                        Swal.fire("Error!", results.message, "error");
                    }
                }
            });

        } else {
            e.dismiss;
        }

    }, function (dismiss) {
        return false;
    })
}

// Input nomor saja
function isNumberKey(evt)
{
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
    return false;

    return true;
}
</script>
@endsection

@section('script')
    <script>
        $(document).ready(function () {
            $('#pass').on('click', function() {
                $('#modals').modal('show');
            });

            $('#form').on('submit', function (event) {
                event.preventDefault();
                $.ajax({
                    url: "{{ route('admin.ganti') }}",
                    method: 'POST',
                    data: $(this).serialize(),
                    dataType: 'json',
                    success: function(data) {
                        if (data.success) {
                            Swal.fire({
                                icon: 'success',
                                type: 'success',
                                text: 'Password changed!',
                            }).then(function () {
                                location.reload();
                            });
                            $('#form').modal('hide');
                        }
                    }
                });
            });
        });
    </script>
@endsection
