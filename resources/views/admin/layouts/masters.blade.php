<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8" />
        <meta name="csrf-token" content="{{ csrf_token() }}"/>
        <title>Versailles | Admin Dashboard</title>
        <script src="{{ asset('js/jquery.js') }}"></script>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="{{ asset('/logo-versailles.png') }}">

        <!-- App css -->
        <link href="{{ asset('dist/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" id="bootstrap-stylesheet" />
        <link href="{{ asset('dist/css/icons.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('dist/css/app.min.css') }}" rel="stylesheet" type="text/css"  id="app-stylesheet" />
        <link href="{{ asset('dist/libs/datatables/dataTables.bootstrap4.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('dist/libs/datatables/buttons.bootstrap4.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('dist/libs/datatables/responsive.bootstrap4.css') }}" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="{{ asset('css/sweetalert2.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/gijgo.min.css') }}">
    </head>
    <style>
        .card-box {
            margin-top: 20px;
        }
    </style>
    <body>
        <!-- Begin page -->
        <div id="wrapper">
            <!-- Topbar Start -->
            @include('admin.partials.head')
            <!-- end Topbar -->


            <!-- ========== Left Sidebar Start ========== -->
            @include('admin.partials.side')
            <!-- Left Sidebar End -->

            <!-- ============================================================== -->
            <!-- Start Page Content here -->
            <!-- ============================================================== -->

            <div class="content-page">
                <div class="content">
                    <!-- Start Content-->
                    <div class="container-fluid">
                        <!-- start page title -->
                        <div class="row">
                            <div class="col-12">
                                @yield('content')
                            </div>
                        </div>
                        <!-- end page title -->

                    </div> <!-- end container-fluid -->

                </div> <!-- end content -->
                @include('admin.partials._pass')
                <!-- Footer Start -->
                @include('admin.partials.foot')
                <!-- end Footer -->
            </div>
            <!-- ============================================================== -->
            <!-- End Page content -->
            <!-- ============================================================== -->
        </div>
        <!-- END wrapper -->
        <!-- Vendor js -->
        <!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script> -->
        <script src="{{ asset('dist/js/vendor.min.js') }}"></script>
        <script src="{{ asset('dist/libs/datatables/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('dist/libs/datatables/dataTables.bootstrap4.min.js') }}"></script>
        <script src="{{ asset('dist/libs/datatables/dataTables.responsive.min.js') }}"></script>
        <script src="{{ asset('dist/libs/datatables/responsive.bootstrap4.min.js') }}"></script>

        <!-- App js -->
        <script src="{{ asset('dist/js/app.min.js') }}"></script>
        <script src="{{ asset('js/sweetalert2.all.min.js') }}"></script>
        <script src="{{ asset('js/gijgo.min.js') }}"></script>
        @yield('script')
    </body>
    </html>
