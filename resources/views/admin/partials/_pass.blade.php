<!-- Modal Change Password -->
<div class="modal fade" id="modals" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Change Password</h4>
            </div>
            <div class="modal-body">
                <form method="post" id="form" class="form-horizontal">
                    @csrf
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="password">Masukkan Password Baru</label>
                                <input type="password" name="password" id="password" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <input type="submit" value="Change" name="aksi" id="aksi" class="btn btn-block btn-primary">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>