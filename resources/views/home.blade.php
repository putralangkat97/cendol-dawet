@if (session('status'))
    {{ session('status') }}
@endif

<div id="message"></div>
<script>
    pageRedirect();
    function pageRedirect(){
        var delay = 2000; // time in milliseconds

        // Tampil pesan
        document.getElementById("message").innerHTML = "Redirecting...";
        
        // redirect
        setTimeout(function(){
            window.location = "/admin_zone";
        }, delay);
    }
</script>