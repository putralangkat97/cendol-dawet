<?php

namespace App\Http\Controllers;

use App\User;
use Validator;
use App\Number;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NumberController extends Controller
{
    public function index() {
        $data = Number::orderBy('tanggal', 'desc')->get();
        return view('admin.index')->with('number', $data);
    }

    public function add(Request $request) {
        $rules = array(
            'tanggal'   => 'required',
            'waktu'     => 'required',
            'no_satu'   => 'required',
            'no_dua'    => 'required',
            'no_tiga'   => 'required',
        );

        $error = Validator::make($request->all(), $rules);

        if($error->fails())
        {
            return response()->json(['errors' => $error->errors()->all()]);
        }

        $form_data = array(
            'tanggal'   => $request->tanggal,
            'waktu'     => $request->waktu,
            'no_satu'   => $request->no_satu,
            'no_dua'    => $request->no_dua,
            'no_tiga'   => $request->no_tiga,
        );

        if (Number::whereDate('tanggal', $request->tanggal)->where('waktu', $request->waktu)->exists()) {
            return response()->json(['error' => 'Data dah ada boss!']);
        } else {
            Number::create($form_data);
        }

        return response()->json(['success' => 'Data Added']);
    }

    public function edit($id) {
        $data = Number::findOrFail($id);
        return response()->json(['result' => $data]);
    }

    public function update(Request $request, Number $number)
    {
        $rules = array(
            'tanggal'   => 'required',
            'waktu'     => 'required',
            'no_satu'   => 'required',
            'no_dua'    => 'required',
            'no_tiga'   => 'required',
        );

        $error = Validator::make($request->all(), $rules);

        if($error->fails())
        {
            return response()->json(['errors' => $error->errors()->all()]);
        }

        $form_data = array(
            'tanggal'   => $request->tanggal,
            'waktu'     => $request->waktu,
            'no_satu'   => $request->no_satu,
            'no_dua'    => $request->no_dua,
            'no_tiga'   => $request->no_tiga,
        );

        if (Number::whereDate('tanggal', $request->tanggal)->where('waktu', $request->waktu)->exists()) {
            return response()->json(['error' => 'Data dah ada boss!']);
        } else {
            Number::whereId($request->hidden_id)->update($form_data);
        }
        
        return response()->json(['success' => 'Data Updated']);
    }

    public function destroy($id)
    {
        $data = Number::where('id', $id)->delete();
        // check data deleted or not
        if ($data == 1) {
            $success = true;
            $message = "Data deleted successfully";
        } else {
            $success = true;
            $message = "Data not found";
        }
        //  Return response
        return response()->json([
            'success' => $success,
            'message' => $message,
        ]);
    }

    public function pass(Request $request) {
        $idUser = User::where('id', Auth::user()->id)->update([
            'password' => bcrypt($request->password),
        ]);

        return response()->json(['success' => $idUser]);
    }
}
