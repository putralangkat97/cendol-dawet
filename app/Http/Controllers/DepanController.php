<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Number;

class DepanController extends Controller
{
    protected function getAllData()
    {
        $tadi      = Carbon::now('+02:00')->format('M j');
        $ytd      = Carbon::yesterday('+02:00')->format('M j');
        $ytd2     = Carbon::yesterday('+02:00')->add(-1, 'day')->format('M j');
        $ytd3     = Carbon::yesterday('+02:00')->add(-2, 'day')->format('M j');
        $ytd4     = Carbon::yesterday('+02:00')->add(-3, 'day')->format('M j');
        $ytd5     = Carbon::yesterday('+02:00')->add(-4, 'day')->format('M j');
        $ytd6     = Carbon::yesterday('+02:00')->add(-5, 'day')->format('M j');
        $ytd7     = Carbon::yesterday('+02:00')->add(-6, 'day')->format('M j');

        $data = $this->allData();
        $pagi = $this->morning();
        $siang = $this->midday();
        $sore = $this->evening();
        $malam = $this->night();
        $all = $this->tgl();
        $pagiTadi = $this->tadiPagi();
        $siangTadi = $this->tadiSiang();
        $soreTadi = $this->tadiSore();
        $malamTadi = $this->tadiMalam();
        return view('index', compact('data', 'pagi', 'siang', 'sore', 'malam', 'all', 'ytd', 'ytd2', 'ytd3', 'ytd4', 'ytd5', 'ytd6', 'ytd7', 'tadi', 'pagiTadi', 'siangTadi', 'soreTadi', 'malamTadi'));
    }

    public function allData()
    {
        $data = Number::all();
        $today = Carbon::now('+02:00')->format('H:i:s a');

        $day1 = Carbon::createFromTime(00, 00, 00, '+02:00')->format('H:i:s a');
        $day2 = Carbon::createFromTime(3, 59, 59, '+02:00')->format('H:i:s a');

        $day3 = Carbon::createFromTime(4, 00, 00, '+02:00')->format('H:i:s a');
        $day4 = Carbon::createFromTime(7, 59, 59, '+02:00')->format('H:i:s a');

        $day5 = Carbon::createFromTime(8, 00, 00, '+02:00')->format('H:i:s a');
        $day6 = Carbon::createFromTime(11, 59, 59, '+02:00')->format('H:i:s a');

        $day7 = Carbon::createFromTime(12, 00, 00, '+02:00')->format('H:i:s a');
        $day8 = Carbon::createFromTime(23, 59, 59, '+02:00')->format('H:i:s a');

        // $times = '';
        if($today >= $day1 && $today <= $day2)
        {
            $data = Number::whereDate('tanggal', Carbon::now('+02:00')->format('Y-m-d'))->where('waktu', '00:00 AM')->get();
            // $times = 'Waktu 1';
        }
        elseif($today >= $day3 && $today <= $day4)
        {
            $data = Number::whereDate('tanggal', Carbon::now('+02:00')->format('Y-m-d'))->where('waktu', '04:00 AM')->get();
            // $times = 'Waktu 2';
        }
        elseif($today >= $day5 && $today <= $day6)
        {
            $data = Number::whereDate('tanggal', Carbon::now('+02:00')->format('Y-m-d'))->where('waktu', '08:00 AM')->get();
            // $times = 'Waktu 3';
        }
        elseif($today >= $day7 && $today <= $day8)
        {
            $data = Number::whereDate('tanggal', Carbon::now('+02:00')->format('Y-m-d'))->where('waktu', '12:00 PM')->get();
            // $times = 'Waktu 4';
        }

        return $data;
    }

    public function morning()
    {
        $pagi = Number::where('waktu', '00:00 AM')->get();
        return $pagi;
    }

    public function midday()
    {
        $siang = Number::where('waktu', '04:00 AM')->get();
        return $siang;
    }

    public function evening()
    {
        $sore = Number::where('waktu', '08:00 AM')->get();
        return $sore;
    }

    public function night()
    {
        $malam = Number::where('waktu', '12:00 PM')->get();
        return $malam;
    }

    public function tgl()
    {
        $all = Number::groupBy('tanggal')->get();
        return $all;
    }

    public function result()
    {
        $results = Number::orderBy('tanggal', 'desc')->get();
        $skr = Carbon::now('+02:00')->format('Y-m-d');
        $now = Carbon::now('+02:00')->format('l, F d');
        $ytd = Carbon::yesterday('+02:00')->format('Y-m-d');
        $tom = Carbon::now('+02:00')->add(1, 'day')->format('Y-m-d');
        return view('result', compact('skr', 'now', 'ytd', 'tom', 'results'));
    }

    public function tadiPagi() {
      if (Carbon::now('+02:00')->format('H:i:s a') >= Carbon::createFromTime(00, 00, 00, '+02:00')->format('H:i:s a')) {
         $pagiTadi = Number::whereDate('tanggal', Carbon::now('+02:00')->format('Y-m-d'))->where('waktu', '00:00 AM')->get();

         foreach ($pagiTadi as $key => $value) {
            return $value->no_satu;
         }
      }
   }

   public function tadiSiang() {
      if (Carbon::now('+02:00')->format('H:i:s a') >= Carbon::createFromTime(04, 00, 00, '+02:00')->format('H:i:s a')) {
         $siangTadi = Number::whereDate('tanggal', Carbon::now('+02:00')->format('Y-m-d'))->where('waktu', '04:00 AM')->get();

         foreach ($siangTadi as $key => $value) {
            return $value->no_satu;
         }
      }
   }

   public function tadiSore() {
      if (Carbon::now('+02:00')->format('H:i:s a') >= Carbon::createFromTime(8, 00, 00, '+02:00')->format('H:i:s a')) {
         $soreTadi = Number::whereDate('tanggal', Carbon::now('+02:00')->format('Y-m-d'))->where('waktu', '08:00 AM')->get();

         foreach ($soreTadi as $key => $value) {
            return $value->no_satu;
         }
      }
   }

   public function tadiMalam() {
      if (Carbon::now('+02:00')->format('H:i:s a') >= Carbon::createFromTime(12, 00, 00, '+02:00')->format('H:i:s a')) {
         $malamTadi = Number::whereDate('tanggal', Carbon::now('+02:00')->format('Y-m-d'))->where('waktu', '12:00 PM')->get();

         foreach ($malamTadi as $key => $value) {
            return $value->no_satu;
         }
      }
   }
}
