<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Number extends Model
{
    protected $dates = ['tanggal'];
    protected $fillable = [
        'tanggal',
        'waktu',
        'no_satu',
        'no_dua',
        'no_tiga'
    ];
}
