<?php

use App\Number;
use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'DepanController@getAllData');
Route::get('/result', 'DepanController@result');

Auth::routes(['register' => false]);

Route::get('/home', 'HomeController@index')->name('home');

Route::name('admin.')->middleware(['auth', 'auth.superadmin'])->group(function() {
    Route::get('/admin_zone', 'NumberController@index')->name('dashboard');
    Route::post('/admin_zone/add', 'NumberController@add')->name('add');
    Route::get('/admin_zone/{id}/edit', 'NumberController@edit');
    Route::post('/admin_zone/update', 'NumberController@update')->name('update');
    Route::post('/admin_zone/destroy/{id}', 'NumberController@destroy');
    Route::post('/admin_zone/ganti', 'NumberController@pass')->name('ganti');
});